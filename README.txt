================================================================================
# DPZ

DPZ - Demo project Zemoga

================================================================================

## Pre-requisitos

Para el despliegue del proyecto se necesita un servidor de aplicaciones Tomcat 8.5.+ (más información en el archivo TomcatConfiguration)
https://tomcat.apache.org/download-80.cgi#8.5.54

Apache Tomcat 8.5 requiere de Java Standard Edition Runtime
Environment (JRE) con versión 7 o superior.

No es necesario el ingreso de pool de conexiones en el servidor de aplicaciones para este caso, ya que la conexión está directa a la BD.

Entrar al administrador del servidor, cargar el archivo WAR y desplegar.

## Tecnologías

Front End
	-HTML/CSS/JS
	-ANGULARJS
Back End
	-Java
Librerías
	-springfox-swagger2
	-spring-boot-starter-web
	-junit
	-mysql-connector-java
	-spring-social-twitter
	-spring-boot-starter-test

##URLs 

SinglePage App
http://{{nombre_del_servidor}}:8080/DPZ/

RESTFUL WEBSERVICES
http://{{nombre_del_servidor}}:8080/DPZ/swagger-ui.html#/

##Tiempo de la solución

Aproximadamente 6 horas

## Repositorio Público
https://bitbucket.org/geney/demoprojectzemoga/src/master/