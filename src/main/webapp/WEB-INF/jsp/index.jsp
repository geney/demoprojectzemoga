<%-- 
    Document   : newjsp
    Created on : 6/11/2018, 09:18:27 AM
    Author     : geneya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html ng-app="app">
    <head>
        <title>DPZ</title>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <base href="/DPZ/" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous" />
        <link rel="stylesheet" href="<c:url value='/assets/css/style.css' />">

    </head>
    <body ng-controller="AppController as vm">
        <%--     ---------------------------------------------------------------------            --%>
        <div class="container">
            <div class="row">
                </br>
                </br>
                </br>
                </br>
                <div class="col-md-4">
                    <div class="card">
                        <img class="card-img-top" src="{{vm.image_url}}" alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text">{{vm.twitter_user_name}}</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            {{vm.title}}'s Timeline
                        </div>
                        <div class="card-body">
                            <div ng-repeat="tweet in tweets" class="card">
                                <div class="card-horizontal">
                                    <div class="img-square-wrapper">
                                        <img class="image-scaled" src="{{tweet.profileImageUrl}}" alt="Card image cap">
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-title">{{tweet.fromUser}}</h6>
                                        <p class="card-text">{{tweet.text}}</p>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <small class="text-muted">{{tweet.createdAt}}</small>
                                </div>
                            </div>
                            <a href="https://twitter.com/{{vm.twitter_user_name}}" class="card-body">Go to Account>></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <h3 class="title-style">{{vm.title}}</h3>                 
                    <div class="card">
                        <div class="card-header">
                            My Work Experience
                        </div>
                        <div class="card-body">
                            <p class="card-text">{{vm.description}}</p>
                        </div>
                        <a href="{{vm.urlServer}}swagger-ui.html#/" class="card-body">Go to API Documentation>></a>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.2/angular.min.js" crossorigin="anonymous"></script>
    <script src="<c:url value='/assets/js/app.file.js' />"></script>
    <script src="<c:url value='/assets/js/portfolio.ng.service.js' />"></script>
    <script src="<c:url value='/assets/js/twitter.ng.service.js' />"></script>
</body>
</html>
