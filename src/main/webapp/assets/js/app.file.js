angular
        .module('app', []);
angular
        .module('app')
        .controller('AppController', AppController);

AppController.$inject = ['$window', 'PortfolioServices','$scope','TwitterServices'];

function AppController($window, PortfolioServices,$scope, TwitterServices) {
    var vm = this;
    var id = 9999;
    var twitterObject = new Object();
    twitterObject.screen_name = "GeneyTest";
    twitterObject.count = 5;

    var regex = new RegExp('/[^/]*$');
    vm.urlServer = $window.location.href.replace(regex, '/');

    $scope.tweets = [];

    function setData() {
        PortfolioServices.getById(id).then(
                function (response) {
                    if (response.status === 200) {
                        if (response.data !== null && !angular.isUndefined(response.data)) {
                            vm.title = response.data.title;
                            vm.description = response.data.description;
                            vm.image_url = response.data.image_url;
                            vm.twitter_user_name = response.data.twitter_user_name;
                        }
                    } else if (response.status === 204) {
                        alert("El id " + id + " no existe en la BD");
                    } else {
                        console.log("Respuesta" + response.status);
                    }
                },
                function (response) {
                    console.log("Error");
                }
        );
    }
    
    function setDataTwitter() {
        TwitterServices.consume(twitterObject).then(
                function (response) {
                    if (response.status === 200) {
                        if (response.data !== null && !angular.isUndefined(response.data)) {
                            $scope.tweets = [];
                            if (Array.isArray(response.data)) {
                                response.data.forEach(function (element) {
                                    //element.createdAt = element.createdAt.toISOString().slice(0, 10);
                                    $scope.tweets.push(element);
                                });
                            } else {
                                var data = response.data;
                                //data.createdAt = data.createdAt.toISOString().slice(0, 10);
                                $scope.tweets.push(data);
                            }
                        }
                    } else if (response.status === 204) {
                        alert("El id " + id + " no existe en la BD");
                    } else {
                        console.log("Respuesta" + response.status);
                    }
                },
                function (response) {
                    console.log("Error");
                }
        );
    }

    setData();
    setDataTwitter();
}