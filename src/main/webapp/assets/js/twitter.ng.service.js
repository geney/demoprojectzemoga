(function () {
    'use strict';

    angular
            .module('app')
            .service('TwitterServices', TwitterServices);

    TwitterServices.$inject = ['$http', '$q'];

    function TwitterServices($http, $q) {
        var objectClass = "twitter";

        var service = {
            consume: consume
        };

        return service;


        function consume(data) {
            return $http.post('api/' + objectClass + '/', data)
                    .then(
                            function (response) {
                                return response;
                            },
                            function (errResponse) {
                                return $q.reject(errResponse);
                            }
                    );
        }
    
    }
})();

