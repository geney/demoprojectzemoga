(function () {
    'use strict';

    angular
            .module('app')
            .service('PortfolioServices', PortfolioServices);

    PortfolioServices.$inject = ['$http', '$q'];

    function PortfolioServices($http, $q) {
        var objectClass = "portfolio";

        var service = {
            getAll: getAll,
            getById: getById,
            create: create,
            edit: edit,
        };

        return service;

        function getAll() {
            return $http.get('api/' + objectClass + '/')
                    .then(
                            function (response) {
                                return response;
                            },
                            function (errResponse) {
                                return $q.reject(errResponse);
                            }
                    );
        }

        function getById(id) {
            return $http.get('api/' + objectClass + '/' + id)
                    .then(
                            function (response) {
                                return response;
                            },
                            function (errResponse) {
                                return $q.reject(errResponse);
                            }
                    );
        }

        function create(data) {
            return $http.post('api/' + objectClass + '/', data)
                    .then(
                            function (response) {
                                return response;
                            },
                            function (errResponse) {
                                return $q.reject(errResponse);
                            }
                    );
        }

        function edit(id, data) {
            return $http.put('api/' + objectClass + '/' + id, data)
                    .then(
                            function (response) {
                                return response;
                            },
                            function (errResponse) {
                                return $q.reject(errResponse);
                            }
                    );
        }
          
    }
})();

