/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zemoga.demo.dpz.repository;

import com.zemoga.demo.dpz.entity.Portfolio;
import org.springframework.data.repository.CrudRepository;
/**
 *
 * @author Geney Alvarez
 */
public interface PortfolioRepository extends CrudRepository<Portfolio, Integer>{
    
}
