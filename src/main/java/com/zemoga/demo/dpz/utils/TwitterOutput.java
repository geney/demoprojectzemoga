/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zemoga.demo.dpz.utils;

import java.util.Date;

/**
 *
 * @author Geney Alvarez
 */
public class TwitterOutput {
    private String fromUser;
    private String text; 
    private String profileImageUrl;
    private Date createdAt;

    public TwitterOutput(String fromUser, String text, String profileImageUrl, Date createdAt) {
        this.fromUser = fromUser;
        this.text = text;
        this.profileImageUrl = profileImageUrl;
        this.createdAt = createdAt;
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
    
    
}
