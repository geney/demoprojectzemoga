/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zemoga.demo.dpz.utils;

/**
 *
 * @author Geney Alvarez
 */
public class TwitterInput {
    private String screen_name;
    private int count;

    public TwitterInput() {
    }

    public TwitterInput(String screen_name, int count) {
        this.screen_name = screen_name;
        this.count = count;
    }

    public String getScreen_name() {
        return screen_name;
    }

    public void setScreen_name(String screen_name) {
        this.screen_name = screen_name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    
    
}
