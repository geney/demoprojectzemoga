/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zemoga.demo.dpz;

import com.google.common.base.Predicates;
import java.util.ArrayList;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.paths.RelativePathProvider;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *
 * @author Geney Alvarez
 */

@Configuration
@EnableSwagger2
public class SpringFoxConfig {

    @Bean
    public Docket api() {
        ApiInfo apiInfo = new ApiInfo(
                "DPZ",
                "Este proyecto contiene los servicios web de DZP - Demo Portafolio Zemoga",
                "1.0",
                "https://www.zemoga.com/",
                null,
                "Página principal",
                "http://192.168.1.17:8080/DPZ/",
                new ArrayList<>());

        return new Docket(DocumentationType.SWAGGER_2)//<3>
                .apiInfo(apiInfo)
                .select()//<4>
                .apis(RequestHandlerSelectors.any())//<5>
                .paths(Predicates.not(PathSelectors.regex("/error.*")))//<6>, regex must be in double quotes.
                .build();
    }
}
