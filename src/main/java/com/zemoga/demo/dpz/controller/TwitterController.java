/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zemoga.demo.dpz.controller;

import com.zemoga.demo.dpz.utils.Constants;
import com.zemoga.demo.dpz.utils.TwitterInput;
import com.zemoga.demo.dpz.utils.TwitterOutput;
import io.swagger.annotations.Api;
import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Geney Alvarez
 */
@Api(tags = "Twitter API")
@RestController
@RequestMapping(Constants.URL_API_INTEGRATION_TWITTER_ROOT)
public class TwitterController {
    
    private Twitter twitter = new TwitterTemplate(Constants.TWITTER_CONSUMER_KEY, 
                                                        Constants.TWITTER_CONSUMER_SECRET, 
                                                        Constants.TWITTER_ACCESS_TOKEN, 
                                                        Constants.TWITTER_ACCESS_TOKEN_SECRET);
    
    @RequestMapping(value = Constants.URL_MAP_ROOT, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TwitterOutput>> findTweets(@RequestBody TwitterInput obj, UriComponentsBuilder ucBuilder) {
        if(obj == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<Tweet> list = twitter.searchOperations().search(obj.getScreen_name(), obj.getCount()).getTweets();
        List<TwitterOutput> output = new ArrayList<>();
        list.forEach((t) -> {
            output.add(new TwitterOutput(t.getFromUser(),t.getText(),t.getProfileImageUrl(),t.getCreatedAt()));
        });
        if(output.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
            return new ResponseEntity<>(output, HttpStatus.OK);
        }
    }
}
