/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zemoga.demo.dpz.controller;

import com.zemoga.demo.dpz.entity.Portfolio;
import com.zemoga.demo.dpz.service.PortfolioService;
import com.zemoga.demo.dpz.utils.Constants;
import io.swagger.annotations.Api;
import java.util.List;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Geney Alvarez
 */
@Api(tags = "Gestiòn de Portafolios")
@RestController
@RequestMapping(Constants.URL_API_INTEGRATION_PORTFOLIO_ROOT)
public class PortfolioController {

    @Autowired
    private PortfolioService service;

    @RequestMapping(value = Constants.URL_MAP_ROOT, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Portfolio>> findPortfolio() {
        try {
            List<Portfolio> objs = service.findAll();
            if (objs.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(objs, HttpStatus.OK);
        } catch (JDBCConnectionException e) {
            return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    @RequestMapping(value = Constants.URL_MAP_FIND_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Portfolio> findPortfolioById(@PathVariable("id") int id) {
        try {
            Portfolio obj = service.find(id);
            if (obj == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(obj, HttpStatus.OK);
        } catch (JDBCConnectionException e) {
            return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
        }    
    }

    @RequestMapping(value = Constants.URL_MAP_ROOT, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createPortfolio(@RequestBody Portfolio obj, UriComponentsBuilder ucBuilder) {
        try {
            service.save(obj);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(ucBuilder.path(Constants.URL_MAP_FIND_ID).buildAndExpand(obj.getIdportfolio()).toUri());
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        } catch (JDBCConnectionException e) {
            return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
        }     
    }

    @RequestMapping(value = Constants.URL_MAP_FIND_ID, method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> editPortfolio(@PathVariable("id") int id, @RequestBody Portfolio obj) {
        try {
            Portfolio objSearch = service.find(id);
            if (objSearch == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else if (obj.getIdportfolio()!=id) {
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }
            service.save(obj);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (JDBCConnectionException e) {
            return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
        } 
    }
 
}
