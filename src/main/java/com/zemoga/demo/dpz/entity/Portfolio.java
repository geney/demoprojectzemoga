/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zemoga.demo.dpz.entity;

import com.zemoga.demo.dpz.utils.Constants;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
/**
 *
 * @author Geney Alvarez
 */
@Entity
@Table(name = Constants.TABLENAME_PORTFOLIO)
@XmlRootElement
public class Portfolio implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idportfolio")
    private int idportfolio;
    @Basic(optional = true)
    @Column(name = "description")
    private String description;
    @Basic(optional = true)
    @Column(name = "image_url")
    private String image_url;
    @Basic(optional = true)
    @Column(name = "twitter_user_name")
    private String twitter_user_name;
    @Basic(optional = true)
    @Column(name = "title")
    private String title;

    public Portfolio() {
    }

    public Portfolio(int idportfolio, String description, String image_url, String twitter_user_name, String title) {
        this.idportfolio = idportfolio;
        this.description = description;
        this.image_url = image_url;
        this.twitter_user_name = twitter_user_name;
        this.title = title;
    }

    public int getIdportfolio() {
        return idportfolio;
    }

    public void setIdportfolio(int idportfolio) {
        this.idportfolio = idportfolio;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getTwitter_user_name() {
        return twitter_user_name;
    }

    public void setTwitter_user_name(String twitter_user_name) {
        this.twitter_user_name = twitter_user_name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

   
}
