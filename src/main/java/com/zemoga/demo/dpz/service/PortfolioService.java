/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zemoga.demo.dpz.service;

import com.zemoga.demo.dpz.entity.Portfolio;
import com.zemoga.demo.dpz.repository.PortfolioRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author Geney Alvarez
 */
@Service
@Transactional
public class PortfolioService {

    private final PortfolioRepository repository;

    public PortfolioService(PortfolioRepository repository) {
        this.repository = repository;
    }

    public Portfolio addPortfolio(Portfolio port) {
        return repository.save(port);
    }

    public List<Portfolio> findAll() {
        List<Portfolio> objs = new ArrayList<>();
        for (Portfolio obj : repository.findAll()) {
            objs.add(obj);
        }
        return objs;
    }

    public Portfolio find(int id) {
        Optional<Portfolio> obj = repository.findById(id);
        return obj.isPresent() ? obj.get() : null;
    }

    public void save(Portfolio obj) {
        repository.save(obj);
    }

}
