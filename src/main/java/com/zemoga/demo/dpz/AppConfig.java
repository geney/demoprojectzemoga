/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zemoga.demo.dpz;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Geney Alvarez
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"com.zemoga.demo.dpz.repository", "com.zemoga.demo.dpz.service"}, entityManagerFactoryRef = "mainDSEmFactory", transactionManagerRef = "mainDSTransactionManager")
public class AppConfig {

    /*
    @Value("${spring.datasource.primary.jndi-name}")
    private String primaryJndiName;

    private final JndiDataSourceLookup lookup = new JndiDataSourceLookup();

    @Primary
    @Bean(destroyMethod = "") 
    public DataSource primaryDs() {
        return lookup.getDataSource(primaryJndiName);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }*/
    @Primary
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSourceProperties mainDatasourceProperties() {
        return new DataSourceProperties();
    }

    @Primary
    @Bean
    public DataSource mainDS(@Qualifier("mainDatasourceProperties") DataSourceProperties mainDatasourceProperties) {
        return mainDatasourceProperties.initializeDataSourceBuilder().build();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean mainDSEmFactory(@Qualifier("mainDS") DataSource mainDS, EntityManagerFactoryBuilder builder) {
        return builder.dataSource(mainDS).packages("com.zemoga.demo.dpz.entity").build();
    }

    @Primary
    @Bean
    public PlatformTransactionManager mainDSTransactionManager(EntityManagerFactory mainDSEmFactory) {
        return new JpaTransactionManager(mainDSEmFactory);
    }
}
