/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.zemoga.demo.dpz.Application;
import com.zemoga.demo.dpz.entity.Portfolio;
import com.zemoga.demo.dpz.repository.PortfolioRepository;
import com.zemoga.demo.dpz.service.PortfolioService;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.apache.http.client.ClientProtocolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.mock.mockito.MockBean;

/**
 *
 * @author geneya
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ConnectionTests {

    private static int cont = 1;
    static final Logger LOGGER = LoggerFactory.getLogger(ConnectionTests.class);

    @Autowired
    private PortfolioService service;

    @MockBean
    private PortfolioRepository repository;

    public ConnectionTests() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        LOGGER.info("-------------------------------------------------------");
        LOGGER.info("Inicio del test #" + cont);
        cont++;
    }

    @After
    public void tearDown() {
        LOGGER.info("Fin del test");
        LOGGER.info("-------------------------------------------------------");
    }
    
    @Test
    public void savePortfolioTest() {
        Portfolio port = new Portfolio(9998, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut imperdiet pharetra dapibus. Curabitur bibendum elit eu tempor lobortis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas pharetra justo vitae nisi dignissim luctus.",
                "https://instagram.fbaq6-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/s640x640/53201534_249349532638510_6025746376952128817_n.jpg?_nc_ht=instagram.fbaq6-1.fna.fbcdn.net&_nc_cat=110&_nc_ohc=5XsDqMJmrEcAX8lp5Bt&oh=b8de226031d1407065dd300f525f23fb&oe=5EE50A84",
                "GeneyTest", "Geney Alvarez");
        when(repository.save(port)).thenReturn(port);
        assertEquals(port, service.addPortfolio(port));
    }

}
